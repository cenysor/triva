Welcome to Triva: Tools for translating Triva Lapur

Website: https://cenysor.neocities.org/um.html

To run this program you need to have python 3 installed as well as the python modules "pillow" and "pygame":

WINDOWS: Download and install python 3: https://www.python.org/downloads
Open a command prompt and install pillow and pygame (replace "user" with your username):

pip install --user pillow
pip install --user pygame

Download triva from above and extract the folder anywhere.
Open a command prompt and run:

python C:\path\to\triva.py


The elements of the program's graphical user interface are explained here:

Input Sentence: Enter the sentence you want to translate here.

Input Gloss: Enter the interlinear gloss of the sentence you want to translate here.

A gloss is a schematic representation of word(compound)s. A word can consist of one or more morphemes.
A morpheme can be thought of as the smallest meaningful unit of a language.
Two or more morphemes in a word are connected by a hyphen.
If more than one definition corresponds to a morpheme these definitions are connected by a dot.
Words are generally written in lower case letters except for proper names which generally begin with a
capital letter and glossing abbrevitations which are generally written in all-caps.
Here is a glossing example:

honored Ashma REL  syllable-ACC.PL royalty-GEN intonation-ACC.PL-and seven know-3SG very proud   accomplishment-ABL.PL 3SG.POSS be -3SG
Prī     Ashma iyam atun    -ara    prashim-am  ampūri    -ra    -sha vrama glūn-am  ju   vardrir lān           -ura    ayam     nik-am

As you can see each morpheme in the gloss corresponds to a single morpheme in the transliteration.

Here is a list of all glossing abbrevitations currently used in the program:

1       1st person
2       2nd person
3       3rd person
PL      plural
VOC     vocative
GEN     genitive
POSS    possessive
ACC     accusative
DAT     dative
LOC     locative
ABL     ablative
ISTR    instrumental
COM     comitative
DEM     demonstrative
Q       interrogative
REL     relative
COMP    comparative
SUPL    superlative
SBJV    subjunctive
FUT     future
CARD    cardinal
DTV     determinative
DET     determiner
BEN     benefactive
NEG     negative

More info: https://en.wikipedia.org/wiki/Interlinear_gloss

If more than one dictionary entry contains the definition you entered a popup will ask you to choose the desired entry.
Enter the corresponding integer and click the "select" button.


Update button: transliterates the input gloss.

Transcr(ibe) button: transcribes the sentence into the logographic script of Triva Lapur.

Export button: opens the export popup:

image name: Enter the file name under which you want to save the transcription

title: Enter the file name under which you want to save the input sentence and transliteration.

blog button: This button exports to html (can not be used currently).

export button: This button saves the image and text file in the "export" directory.


In the lower half of the main window of the program's GUI there are the following elements:

Random button: generates a random word (for inspiration).

Entry: Enter the word in Triva Lapur you want to add to the dictionary.
Long vowels must be written as double vowels (ā: aa, etc.).

Grammar: Enter a short description of the grammatical function of the word you want to add.

Exaples for grammatical descriptions are: 

n.      noun
v.      verb
adj.    adjective
adv.    adverb
pron.   pronoun
prep.   preposition
num.    numeral
conj.   conjunction


Character: Enter the name of the logographic character that the word you want to add will use.
If you leave this blank, the word itself is used as the name for the character.
You can also enter the name of an existing character if you want the word to use the same one.

Definition: Enter one or more definitions of the word you want to add in English.
Definitions must be separated by commas. You can put optional definitions in parentheses.
Ex.: "but (also)"

Add button: adds the word to the dictionary.
If you did not enter an existing character the character design popup will open.
Use the mouse to move the cursor over the 12x12 pixel matrix.
With the left mouse button you can paint and delete pixels.
If you leave the rightmost column of pixels blank it will be cut off
(This is useful for designing characters with vertical symmetry).
If you close the character design popup the character will be saved and associated with the word you added.
If the character already exists a popup will warn you.

Delete button: deletes the entry in the entry field from the dictionary.
It is not necessary to specify the other inputs.

If you input an existing word into the entry field you can add additional definitions to the entry
by entering them into the definitions field (one at a time) and clicking the add button.
If you don't leave the grammar or character field blank a new entry with these specifications will be added instead.

To do:
- Add window titles
- Rewrite the character designer in tkinter
- Add option to export gloss

